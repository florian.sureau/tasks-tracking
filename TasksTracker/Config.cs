﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksTracker
{
    internal static class Config
    {
        public static string GetCurrentDate()
        {
            return DateTime.Now.ToString("MM/dd/yyyy");
        }
        public static UserParameters Get()
        {
            var parameters = JsonConvert.DeserializeObject<UserParameters>(Properties.Settings.Default.Data);
            parameters = parameters ?? new UserParameters();
            if (!parameters.Data.ContainsKey(GetCurrentDate()))
            {
                parameters.Data.Add(GetCurrentDate(), new List<TaskElement> { new TaskElement { Name = "None", Time = 0 } });
                Set(parameters);
            }
            if(parameters.Data.Last().Value.FirstOrDefault(mbox=>mbox.Name.ToLower() == "none") == null)
            {
                parameters.Data.Last().Value.Add(new TaskElement { Name = "None", Time = 0 });
                Set(parameters);
            }
            return parameters;
        }

        public static void Set(UserParameters parameters)
        {
            Properties.Settings.Default.Data = JsonConvert.SerializeObject(parameters);
            Properties.Settings.Default.Save();
        }
    }
}
