﻿using System.Collections.Generic;

namespace TasksTracker
{
    public class UserParameters
    {
        public IDictionary<string, IList<TaskElement>> Data = new Dictionary<string, IList<TaskElement>>();

        public UserParameters()
        {
        }
    }
}
