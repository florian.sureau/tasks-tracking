﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace TasksTracker
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class TasksUC : UserControl
    {
        public ObservableCollection<TaskElement> Data { get; set; } = new ObservableCollection<TaskElement>();
        private TaskElement SelectedElement { get; set; }
        private DateTime _initialTime = DateTime.Now;
        private TaskElement _lockSessionElement;

        public TasksUC()
        {
            InitializeComponent();

            LoadData();
            DataContext = Data;

            SelectedElement = Data.First();
            _initialTime = DateTime.Now;
            listView.SelectedIndex = 0;

            SystemEvents.SessionSwitch += new SessionSwitchEventHandler(SystemEventsSessionSwitch);
        }

        #region events

        void SystemEventsSessionSwitch(object sender, SessionSwitchEventArgs e)
        {
            if (e.Reason == SessionSwitchReason.SessionLock)
            {
                _lockSessionElement = SelectedElement;
                listView.SelectedItem = Data.FirstOrDefault(m => m.Name.StartsWith("None"));
                Debug.WriteLine("Lock screen");
            }
            else if (e.Reason == SessionSwitchReason.SessionUnlock)
            {
                listView.SelectedItem = Data.FirstOrDefault(m => m == _lockSessionElement);
                _lockSessionElement = null;
                Debug.WriteLine("Unlock screen");
            }
        }
        private void OnAddTaskBtnClick(object sender, RoutedEventArgs e)
        {
            var dial = new EditElement();
            if (dial.ShowDialog() == true)
            {
                AddTask(dial.ElementName, 0);
            }
        }

        private void SelectionChangedEvent(object sender, SelectionChangedEventArgs e)
        {
            DateTime currentTime = DateTime.Now;
            if (e.RemovedItems.Count > 0)
            {
                (e.RemovedItems[0] as TaskElement).Time += currentTime.Ticks - _initialTime.Ticks;
                _initialTime = currentTime;
                SelectedElement = e.AddedItems[0] as TaskElement;
            }

            SaveData();
        }

        private void OnInitElementClick(object sender, RoutedEventArgs e)
        {
            ((sender as Button).DataContext as TaskElement).Time = 0;
        }

        private void OnDeleteElementClick(object sender, RoutedEventArgs e)
        {
            Data.Remove((sender as Button).DataContext as TaskElement);
        }

        private void OnLinkElementClick(object sender, RoutedEventArgs e)
        {
            var link = ((sender as Button).DataContext as TaskElement).Link;
            if (!String.IsNullOrEmpty(link))
                System.Diagnostics.Process.Start(link);
        }

        private void OnEditElementClick(object sender, RoutedEventArgs e)
        {
            SaveData();

            TaskElement selectedElement = (sender as Button).DataContext as TaskElement;
            EditElement dial = new EditElement { ElementName = selectedElement.Name, ElementDate = selectedElement.Time };
            if (dial.ShowDialog() == true)
            {
                selectedElement.Name = dial.ElementName;
                selectedElement.Time = dial.ElementDate;
                selectedElement.Link = dial.Link;
            }
        }

        private void OnExportBtnClick(object sender, RoutedEventArgs e)
        {
            Export();
        }
        private void OnResetBtnClick(object sender, RoutedEventArgs e)
        {
            ResetTime();
        }

        private void OnImportClick(object sender, RoutedEventArgs e)
        {
            Import();
        }

        #endregion

        #region Methods

        internal void Close()
        {
            SaveData();
        }

        private void LoadData()
        {
            Data.Clear();
            var date = Config.GetCurrentDate();
            var parameters = Config.Get();
            var newData = parameters.Data.ContainsKey(date) ? new ObservableCollection<TaskElement>(parameters.Data[date]) : new ObservableCollection<TaskElement>();
            foreach (var item in newData)
            {
                Data.Add(item);
            }
        }

        private void Import()
        {
            // save user values
            SaveData();

            var dlg = new OpenFileDialog();

            // set default folder
            if (Directory.Exists(Properties.Settings.Default.DefaultExportFolder))
                dlg.InitialDirectory = Properties.Settings.Default.DefaultExportFolder;

            // set dialog extension filter
            dlg.DefaultExt = ".json"; // Default file extension
            dlg.Filter = "JSON document (.json)|*.json"; // Filter files by extension


            if (dlg.ShowDialog() != true) return;

            Config.Set(JsonConvert.DeserializeObject<UserParameters>(File.ReadAllText(dlg.FileName)));
            LoadData();
        }

        internal void SaveData()
        {
            var date = Config.GetCurrentDate();
            var parameters = Config.Get();

            parameters.Data[date] = Data.ToList();
            Config.Set(parameters);

            timeColumn.Header = "Time (" + new DateTime(Data.Where(m => m.CanDelete).Sum(item => item.Time)).ToString("H:mm:ss") + ")";
        }

        private void AddTask(String name, long time)
        {
            Data.Add(new TaskElement() { Name = name, Time = time });
        }

        private void ResetTime()
        {
            foreach (var item in this.Data)
            {
                item.Reset();
            }
            listView.SelectedItem = Data.FirstOrDefault(m => m.Name.StartsWith("None"));
        }

        private void Export()
        {
            var dlg = new SaveFileDialog();

            // save user values
            SaveData();

            // set default folder
            if (Directory.Exists(Properties.Settings.Default.DefaultExportFolder))
                dlg.InitialDirectory = Properties.Settings.Default.DefaultExportFolder;

            // set dialog extension filter
            dlg.DefaultExt = ".json"; // Default file extension
            dlg.Filter = "JSON document (.json)|*.json"; // Filter files by extension

            // set default filename
            dlg.FileName = DateTime.Now.ToString("dd-MM-yyyy") + ".json";

            if (dlg.ShowDialog() != true) return;

            File.WriteAllText(dlg.FileName, Properties.Settings.Default.Data);
        }

        #endregion
    }
}
