﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;

namespace TasksTracker
{
    [SettingsSerializeAs(SettingsSerializeAs.Xml)]
    public class TaskElement : INotifyPropertyChanged
    {
        public TaskElement()
        {
            Time = DateTime.Now.Ticks;
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Notify(nameof(Name));
                Notify("IsNoneElement");
            }
        }

        private long _time;
        public long Time
        {
            get { return _time; }
            set
            {
                _time = value;
                Notify(nameof(Time));
                Notify(nameof(Date));
            }
        }

        private string _link;

        public string Link
        {
            get { return _link ?? ""; }
            set
            {
                _link = value;
                Notify(nameof(Link));
            }
        }


        [JsonIgnore]
        public bool CanDelete
        {
            get
            {
                return this.Name.ToLower() != "none";
            }
        }

        public DateTime Date { get { return new DateTime(Time); } }

        public override string ToString()
        {
            return Name + " [" + new DateTime(Time) + "]";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Notify(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Reset()
        {
            Time = 0;
        }
    }
}
