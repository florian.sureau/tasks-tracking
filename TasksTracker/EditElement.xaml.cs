﻿using System;
using System.Windows;

namespace TasksTracker
{
    /// <summary>
    /// Interaction logic for EditElement.xaml
    /// </summary>
    public partial class EditElement : Window
    {
        public String ElementName { get { return NameTB.Text; } set { NameTB.Text = value; } }

        public long ElementDate
        {
            get { return timePickerTP.SelectedTime.Ticks; }
            set
            {
                addEditBTN.Content = "Edit";
                timePickerTP.SelectedTime = new TimeSpan(value);
            }
        }

        public string Link
        {
            get { return LinkTB.Text; }
            set { LinkTB.Text = value; }
        }

        public EditElement()
        {
            InitializeComponent();
            timePickerTP.SelectedTime = new TimeSpan(0);
            addEditBTN.Content = "Add";
        }

        private void OnAddEditClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
