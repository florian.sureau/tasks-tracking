﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Windows;

namespace TasksTracker
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly System.Windows.Forms.NotifyIcon notifyIcon;
        private readonly System.Windows.Forms.ContextMenu contextMenu;
        private readonly System.Windows.Forms.MenuItem exitMenuItem;
        private readonly System.Windows.Forms.MenuItem saveMenuItem;
        private readonly System.ComponentModel.IContainer components;

        public MainWindow()
        {
            InitializeComponent();

            StateChanged += new EventHandler(OnMainWindowStateChanged);

            components = new System.ComponentModel.Container();
            contextMenu = new System.Windows.Forms.ContextMenu();
            exitMenuItem = new System.Windows.Forms.MenuItem();
            saveMenuItem = new System.Windows.Forms.MenuItem();

            // Initialize contextMenu
            contextMenu.MenuItems.AddRange(new[] { saveMenuItem, exitMenuItem });

            // Initialize menuItem1
            exitMenuItem.Index = 1;
            exitMenuItem.Text = "E&xit";
            exitMenuItem.Click += (sender, e) => Close();

            // Initialize Save menuitem
            saveMenuItem.Index = 0;
            saveMenuItem.Text = "Save";
            saveMenuItem.Click += (sender, e) => tasksUC.SaveData();

            // Create the NotifyIcon.
            notifyIcon = new System.Windows.Forms.NotifyIcon(components)
            {

                // The Icon property sets the icon that will appear
                // in the systray for this application.
                Icon = new Icon("Images/clock.ico"),

                // The ContextMenu property sets the menu that will
                // appear when the systray icon is right clicked.
                ContextMenu = contextMenu,

                // The Text property sets the text that will be displayed,
                // in a tooltip, when the mouse hovers over the systray icon.
                Text = Title,
                Visible = true
            };

            // Handle the DoubleClick event to activate the form.
            notifyIcon.DoubleClick += OnNotifyIconDoubleClick;
        }

        private void OnMainWindowStateChanged(object sender, EventArgs e)
        {
            ShowInTaskbar = WindowState != WindowState.Minimized;
        }

        private void OnNotifyIconDoubleClick(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                WindowState = WindowState.Normal;
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            tasksUC.Close();

            // Clean up any components being used.
            if (components != null)
                components.Dispose();
        }
    }
}
