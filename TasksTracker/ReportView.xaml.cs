﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace TasksTracker
{
    public partial class ReportView : UserControl
    {
        public UserParameters UserParameters { get; set; }
        public ObservableCollection<TaskElement> Data { get; set; } = new ObservableCollection<TaskElement>();

        public ReportView()
        {
            InitializeComponent();
            Refresh();
            this.DataContext = Data;
        }

        private void OnRefreshClick(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        public void Refresh()
        {
            Data.Clear();
            UserParameters = Config.Get();
            foreach (var element in UserParameters.Data)
            {
                foreach (var item in element.Value)
                {
                    var data = Data.Where(m => m != null).FirstOrDefault(m => m.Name.ToLower() == item.Name.ToLower());
                    if (data == null)
                        Data.Add(item);
                    else
                        data.Time += item.Date.Ticks;
                }
            }
        }
    }
}
